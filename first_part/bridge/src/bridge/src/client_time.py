import cv2
import numpy as np
import socket
import sys
import pickle
import struct
import time
from vidgear.gears import VideoGear

#cap = cv2.VideoCapture(0)
cap = VideoGear(source='input.mp4').start()

i=0

while (True):
	frame =cap.read()
	data = pickle.dumps(frame)
	message_size = struct.pack("L", len(data))
	cv2.imshow("frame", message_size)
	key = cv2.waitKey(1)
	if key & 0xFF == ord('q'):
		break
	named_tuple = time.localtime()
	time_string = time.strftime("%H:%M:%S", named_tuple)
	i+=1
	print("Frame ",i,"sent:",time_string )

cap.release()
cv2.destroyAllWindows()
