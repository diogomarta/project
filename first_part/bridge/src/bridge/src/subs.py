#!/usr/bin/env python3

import numpy as np
import pandas as pd
import time
import torch
import torch.nn as nn
import cv2
from utility import *
from yolo import Darknet
import random
import argparse
import pickle as pkl
import socket
import struct
import rospy
import roslib
import os

from sensor_msgs.msg import Image

from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

global classes
global colors
global pub

def arg_parse():
    """Parsing arguments"""
    parser = argparse.ArgumentParser(description='YOLO v3 Real Time Detection')
    parser.add_argument("--confidence", dest="confidence", help="Object Confidence to filter predictions", default=0.25)
    parser.add_argument("--nms_thresh", dest="nms_thresh", help="NMS Threshhold", default=0.4)
    parser.add_argument("--reso", dest='reso', help=
    "Input resolution of the network. Increase to increase accuracy. Decrease to increase speed",
                        default="160", type=str)
    return parser.parse_args()


def prep_image(img, inp_dim):
    """Converting a numpy array of frame into PyTorch tensor"""
    orig_im = img
    dim = orig_im.shape[1], orig_im.shape[0]
    img = cv2.resize(orig_im, (inp_dim, inp_dim))
    img_ = img[:, :, ::-1].transpose((2, 0, 1)).copy()
    img_ = torch.from_numpy(img_).float().div(255.0).unsqueeze(0)
    return img_, orig_im, dim


def write(x, img):
    global classes
    global colors
    c1 = tuple(x[1:3].int())
    c2 = tuple(x[3:5].int())
    cls = int(x[-1])
    label = "{0}".format(classes[cls])
    color = random.choice(colors)
    cv2.rectangle(img, c1, c2, color, 1)
    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1, 1)[0]
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    cv2.rectangle(img, c1, c2, color, -1)
    cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4),
                cv2.FONT_HERSHEY_PLAIN, 1, [225, 255, 255], 1);
    return img

def callback(data):
	global classes
	global colors
	global pub

	cfgfile = 'src/teste/src/cfg/yolov3.cfg'
	weightsfile = "src/teste/src/weight/yolov3.weights"
	num_classes = 80

	args = arg_parse()
	confidence = float(args.confidence)
	nms_thesh = float(args.nms_thresh)
	start = 0

	num_classes = 80
	bbox_attrs = 5 + num_classes

	model = Darknet(cfgfile).to(device)
	model.load_weights(weightsfile)

	model.network_info["height"] = args.reso
	inp_dim = int(model.network_info["height"])

	assert inp_dim % 32 == 0
	assert inp_dim > 32

	model.eval()

	#while True:

	frame = bridge.imgmsg_to_cv2(data, "passthrough")

	img, orig_im, dim = prep_image(frame, inp_dim)
	img.to(device)

	output = model(img)
	output = write_results(output, confidence, num_classes, nms_conf=nms_thesh)


	output[:, 1:5] = torch.clamp(output[:, 1:5], 0.0, float(inp_dim)) / inp_dim

	output[:, [1, 3]] *= frame.shape[1]
	output[:, [2, 4]] *= frame.shape[0]

	classes = load_classes('src/teste/src/data/coco.names')
	colors = pkl.load(open("src/teste/src/color/pallete", "rb"))

	list(map(lambda x: write(x, orig_im), output))
	image_message = bridge.cv2_to_imgmsg(orig_im, encoding = "passthrough")
	pub.publish(image_message)


def listener():
    global pub
    rospy.init_node("video_subscriber", anonymous=True)
    pub = rospy.Publisher("final_video", Image, queue_size=0)
    rospy.Subscriber("video",Image,callback)
    rospy.spin()

if __name__ == "__main__":
    listener()

