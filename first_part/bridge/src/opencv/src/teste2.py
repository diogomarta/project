#! /usr/bin/env python

import cv2
import sys

cap = cv2.VideoCapture(0)

while True:
	ret,frame = cap.read()
	print ("frame")
	cv2.imshow('cam',frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()
