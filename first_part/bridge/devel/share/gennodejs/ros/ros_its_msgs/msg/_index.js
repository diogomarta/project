
"use strict";

let platoon_dist = require('./platoon_dist.js');
let OMNET_CAM = require('./OMNET_CAM.js');
let CAM_simplified = require('./CAM_simplified.js');

module.exports = {
  platoon_dist: platoon_dist,
  OMNET_CAM: OMNET_CAM,
  CAM_simplified: CAM_simplified,
};
