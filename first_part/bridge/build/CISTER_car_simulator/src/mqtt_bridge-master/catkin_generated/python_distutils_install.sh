#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/diogo/catkin_ws/src/CISTER_car_simulator/src/mqtt_bridge-master"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/diogo/catkin_ws/install/lib/python3/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/diogo/catkin_ws/install/lib/python3/dist-packages:/home/diogo/catkin_ws/build/lib/python3/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/diogo/catkin_ws/build" \
    "/usr/bin/python3" \
    "/home/diogo/catkin_ws/src/CISTER_car_simulator/src/mqtt_bridge-master/setup.py" \
    build --build-base "/home/diogo/catkin_ws/build/CISTER_car_simulator/src/mqtt_bridge-master" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/diogo/catkin_ws/install" --install-scripts="/home/diogo/catkin_ws/install/bin"
