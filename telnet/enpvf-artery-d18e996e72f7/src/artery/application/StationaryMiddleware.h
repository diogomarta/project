/*
 * Artery V2X Simulation Framework
 * Copyright 2014-2017 Raphael Riebl
 * Licensed under GPLv2, see COPYING file for detailed license and warranty terms.
 */

#ifndef ARTERY_STATIONARYMIDDLEWARE_H_IE9M1YJ3
#define ARTERY_STATIONARYMIDDLEWARE_H_IE9M1YJ3

#include "artery/rosomnet/Middleware.h"
#include "traci/Listener.h"

#include "artery/rosomnet/VehicleDataProvider.h"
#include "artery/rosomnet/MobilityROS.h"

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <omnetpp.h>
#include <ros_its_msgs/CAM_simplified.h>
#include "artery/rosomnet/ROSOMNeT.h"


namespace artery
{

class StationaryMiddleware : public Middleware, public traci::Listener
{
	public:
		void initialize(int stage) override;
		void receiveSignal(cComponent*, omnetpp::simsignal_t, cObject*, cObject* = nullptr) override;

	protected:
		void initializeManagementInformationBase(vanetza::geonet::MIB&) override;
		//void traciInit() override;

	private:
		//void initializePosition();
		//traci::VehicleController* mVehicleController;
		//VehicleDataProvider mVehicleDataProvider;
		//INET_API::inet::IMobility* getMobilityModule();
		//MobilityROS* getMobilityModule();
};

} // namespace artery

#endif /* ARTERY_STATIONARYMIDDLEWARE_H_IE9M1YJ3 */