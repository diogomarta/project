//
// This file is part of an OMNeT++/OMNEST simulation example.
//
// Copyright (C) 1992-2015 Andras Varga
//
// This file is distributed WITHOUT ANY WARRANTY. See the file
// `license' for details on this and other legal matters.
//

#include <omnetpp/platdep/sockets.h>
#include <omnetpp.h>
#include "artery/rosomnet/TelnetPkt_m.h"
#include "artery/rosomnet/SocketRTScheduler.h"
#include <fstream>
#include <chrono>
#include <unistd.h>
#include <iostream>

using namespace omnetpp;
using namespace std;

class ExtTelnetClient : public cSimpleModule
{
  private:
    cMessage *rtEvent;
    cSocketRTScheduler *rtScheduler;

    char recvBuffer[4000];
    int numRecvBytes;

    int addr;
    int srvAddr;

  public:
    ExtTelnetClient();
    virtual ~ExtTelnetClient();

  protected:
    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    void handleSocketEvent();
    void handleReply(TelnetPkt *telnetReply);
};

Define_Module(ExtTelnetClient);

ExtTelnetClient::ExtTelnetClient()
{
    rtEvent = nullptr;
}

ExtTelnetClient::~ExtTelnetClient()
{
    cancelAndDelete(rtEvent);
}

void ExtTelnetClient::initialize()
{
    cout << "extTelnetClient initialize" << endl;
    rtEvent = new cMessage("rtEvent");
    rtScheduler = check_and_cast<cSocketRTScheduler *>(getSimulation()->getScheduler());

    rtScheduler->setInterfaceModule(this, rtEvent, recvBuffer, 4000, &numRecvBytes);
    
    addr = par("addr");
    srvAddr = par("srvAddr");
}

void ExtTelnetClient::handleMessage(cMessage *msg)
{
    if (msg == rtEvent)
        handleSocketEvent();
    else
       handleReply(check_and_cast<TelnetPkt *>(msg));
}

void ExtTelnetClient::handleSocketEvent()
{
    // get data from buffer
    std::string text = std::string(recvBuffer, numRecvBytes);
    numRecvBytes = 0;
    //const char *reply = telnetReply->getPayload();
    // assemble and send Telnet packet
    TelnetPkt *telnetPkt = new TelnetPkt();
    telnetPkt->setPayload(text.c_str());
    telnetPkt->setName(text.c_str());
    telnetPkt->setDestAddress(srvAddr);
    telnetPkt->setSrcAddress(addr);
    std::cout << "handle_Socket_Event" << std::endl;
    send(telnetPkt, "g$o");
}

void ExtTelnetClient::handleReply(TelnetPkt *telnetReply)
{
    const char *reply = telnetReply->getPayload(); 
    rtScheduler->cSocketRTScheduler::sendBytes(reply, strlen(reply));
    std::ofstream myfile;
    myfile.open("sim_lat_rsu", std::ofstream::out | std::ofstream::app);
    auto t0 = simTime();
    myfile << t0 << std::endl;
    myfile.close();
	
    std::cout << "client receive:" << reply << std::endl;
 
    //usleep(100);
    //handleSocketEvent(telnetReply);
    delete telnetReply;

}

