/*
 * Artery V2X Simulation Framework
 * Copyright 2014-2017 Raphael Riebl
 * Licensed under GPLv2, see COPYING file for detailed license and warranty terms.
 */

#include "artery/inet/RSU.h"
#include "artery/traci/ControllableVehicle.h"
#include "artery/traci/MobilityBase.h"
#include "inet/common/ModuleAccess.h"
#include <vanetza/common/position_fix.hpp>
#include "artery/rosomnet/ROSOMNeT.h"
#include "inet/mobility/base/MovingMobilityBase.h"
#include "artery/rosomnet/Asn1PacketVisitor.h"
#include <fstream>
#include <chrono>
#include "artery/rosomnet/TelnetPkt_m.h"

#include "artery/rosomnet/ItsG5PromiscuousService.h"
#include "artery/rosomnet/ItsG5Service.h"
#include "artery/messages/GeoNetPacket_m.h"
#include "artery/netw/GeoNetIndication.h"
#include "artery/netw/GeoNetRequest.h"
#include "artery/nic/RadioDriverBase.h"
#include "artery/utility/IdentityRegistry.h"
#include "artery/utility/FilterRules.h"
#include <vanetza/btp/header.hpp>
#include <vanetza/btp/header_conversion.hpp>
#include <vanetza/btp/ports.hpp>
#include <vanetza/geonet/data_confirm.hpp>
#include <vanetza/geonet/packet.hpp>
#include <vanetza/geonet/position_vector.hpp>
#include <vanetza/security/naive_certificate_provider.hpp>
#include <vanetza/security/null_certificate_provider.hpp>
#include <vanetza/security/null_certificate_validator.hpp>
#include <vanetza/net/mac_address.hpp>
#include <algorithm>
#include <string>

using namespace std;
using namespace ros;

static const simsignal_t scSignalCamReceived = cComponent::registerSignal("CamReceived");

namespace artery
{

Define_Module(RSU)

/*RobotMiddleware::RobotMiddleware() : 
		nameSpace("/robot_" + to_string(instanceCounter++) + "/"), rosomnet(ROSOMNeT::getInstance()) {
		//nameSpace("/robot_" + to_string(ii++) + "/"), rosomnet(ROSOMNeT::getInstance()) {

	cout << "RobotMiddleware constructor: " << nameSpace << endl;
	//timerMessage = new cMessage(TIMER_MESSAGE);
}*/
int RSU::instanceCounter = 1;

RSU::RSU() : 
rosomnet(ROSOMNeT::getInstance("MID")), nameSpace("/MID_" + to_string(instanceCounter++) + "/")
{
	cout << "RSU constructor" << endl;
}

void RSU::initialize(int stage)
{

	if (stage == 0) {

		
		//findHost()->subscribe(INET_API::MobilityBase::stateChangedSignal, this);
		//getFacilities().register_const(&mVehicleDataProvider);
		//initializeVehicleController();
		//findHost()->subscribe(scSignalCamReceived,this);
		
		/*string ROStopic = std::string("/car") + nameSpace.at(5) + "/omnetCAM";

		std::stringstream rx_omnet_topic;
		rx_omnet_topic << "/car" << nameSpace.at(5) << "/RXNetwork_OMNET";

		cout << "RM advertises " << rx_omnet_topic.str() << endl;

		RXNetSubscriber = rosomnet.getROSNode().subscribe(ROStopic,TOPIC_QUEUE_LENGTH, &RSU::RXNetCallback ,this);

		//Publish (CAM simplified messages) to RX car topic
		pub_RX_omnet	                   = rosomnet.getROSNode().advertise<ros_its_msgs::CAM_simplified>(rx_omnet_topic.str(), 1);*/
		
	} else if (stage == 1) {
		//mVehicleDataProvider.update(mVehicleController);
		//updatePosition();
	}

	Middleware::initialize(0);
	mRadioDriver = inet::findModuleFromPar<RadioDriverBase>(par("radioDriverModule"), findHost());
	mRadioDriverIn = gate("radioDriverIn");
	mRadioDriverOut = gate("radioDriverOut");
}

void RSU::finish()
{
	Middleware::finish();
	findHost()->unsubscribe(INET_API::MobilityBase::stateChangedSignal, this);
}

void RSU::initializeIdentity(Identity& id)
{
	Middleware::initializeIdentity(id);
	//id.traci = mVehicleController->getVehicleId();
	//id.application = mVehicleDataProvider.station_id();
}

void RSU::initializeManagementInformationBase(vanetza::geonet::MIB& mib)
{
	using vanetza::geonet::StationType;
	Middleware::initializeManagementInformationBase(mib);

	mGnStationType = StationType::PASSENGER_CAR;
	//mVehicleDataProvider.setStationType(mGnStationType);

}

void RSU::initializeVehicleController()
{

}

/*void RSU::RXNetCallback(const ros_its_msgs::OMNET_CAM &msg) {
    
    car_name = msg.car_name;
    ros_x = msg.latitude;
    ros_y = msg.longitude;
    ros_z = msg.altitude;
    heading = msg.heading;

	//RSU::setPosition(ros_x + 1000, ros_y + 300, ros_z); //OMNET axis offset
}*/

void RSU::receiveSignal(cComponent* source, simsignal_t signal, cObject *obj, cObject*)
{

	if (signal == scSignalCamReceived) {
        auto* cam = dynamic_cast<CaObject*>(obj);
        if (cam) {
            /*uint32_t stationID = cam->asn1()->header.stationID;

				if (stationID == 1804289383 && mVehicleDataProvider.station_id() == 846930886){
				std::ofstream myfile;
				myfile.open("CAMReceivedCar2.csv", std::ofstream::out | std::ofstream::app);
				//myfile << ros::Time::now().toNSec() * 0.000000001  << std::endl;
    			auto t0 = std::chrono::high_resolution_clock::now();        
    			auto nanosec = t0.time_since_epoch();
				myfile << nanosec.count() << std::endl;
				myfile.close();
				}*/

			double Speed = cam->asn1()->cam.camParameters.highFrequencyContainer.choice.basicVehicleContainerHighFrequency.speed.speedValue;
			Speed = Speed * 0.01;
			double Heading = cam->asn1()->cam.camParameters.highFrequencyContainer.choice.basicVehicleContainerHighFrequency.heading.headingValue;
			Heading = (Heading * M_PI / 180.00000) - M_PI ; //to rad * 0.1
			double latitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.latitude / 1000000.000 ;
			//latitude = latitude * M_PI / 180;
			double longitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.longitude / 1000000.000;
			//longitude = longitude * M_PI / 180;
			double altitude = cam->asn1()->cam.camParameters.basicContainer.referencePosition.altitude.altitudeValue;
			
			double R = 6731;//km

			double xx = R * cos (latitude) * cos (longitude);
			double yy =	R * cos (latitude) * sin (longitude);
			double zz = R * sin (latitude);

			//EV_WARN << "CAM RECEPTION" << std::endl;
			//EV_WARN << "station ID:" << stationID << std::endl;
			//EV_WARN << "latitude:" << latitude << std::endl; //FINISHED
			//EV_WARN << "longitude:" << longitude << std::endl; //FINISHED
			//EV_WARN << "altitude:" << altitude << std::endl;
			//EV_WARN << "X:" << xx << std::endl;
			//EV_WARN << "Y:" << yy << std::endl;
			//EV_WARN << "speed:"<< Speed << std::endl; //m/s FINISHED
			EV_WARN << "heading:"<< Heading << std::endl; //rad FINISHED

			ros_its_msgs::CAM_simplified msg_to_send;

			/*if (stationID == 846930886)
			msg_to_send.car_name = "/car2/";
			else if (stationID == 1804289383 )
			msg_to_send.car_name = "/car1/";
			else if (stationID == 1681692777 )
			msg_to_send.car_name = "/car3/";
			

  			msg_to_send.Station_ID = to_string(stationID);
			msg_to_send.latitude =  latitude;
  			msg_to_send.longitude = longitude;
  			msg_to_send.altitude_altitudeValue = zz;
  			msg_to_send.heading_headingValue = Heading; //FINISHED
  			msg_to_send.speed_speedValue = Speed; //FINISHED*/



  			//msg_to_send.driveDirection = strtof((msgFields[6]).c_str(),0);
  			//msg_to_send.steeringWheelAngle_steeringWheelAngleValue = strtof((msgFields[2]).c_str(),0);

			//pub_RX_omnet.publish(msg_to_send);
			
			/*TelnetPkt *telnetPkt = new TelnetPkt("x");
		        telnetPkt->setPayload("x");
		        telnetPkt->setDestAddress(1);
		        telnetPkt->setSrcAddress(2);

		        send(telnetPkt, "g$o");*/




        } else {
            EV_ERROR << "received signal has no CaObject";
        }
    }

}

void RSU::update()
{
	updatePosition();
	//mVehicleDataProvider.update(mVehicleController);
	Middleware::update();
}

void RSU::updatePosition()
{
	using namespace vanetza::units;
	static const TrueNorth north;
	vanetza::PositionFix position_fix;
	position_fix.timestamp = getRuntime().now();


	position_fix.latitude = GeoAngle::from_value(ros_x);
	position_fix.longitude = GeoAngle::from_value(ros_y);

	position_fix.confidence.semi_minor = 5.0 * si::meter;
	position_fix.confidence.semi_major = 5.0 * si::meter;

	position_fix.course.assign(north + GeoAngle::from_value(heading), north + 3.0 * degree);
	//position_fix.speed.assign(mVehicleDataProvider.speed(), 1.0 * si::meter_per_second);
	

	getRouter().update_position(position_fix);
}

void RSU::setPosition(const double x, const double y, const double z) {

	/*MobilityROS *mobility = getMobilityModule();

	if (mobility != NULL) {
		
		//Coord currentPosition = mobility->getCurrentPosition();
		Coord newPosition = Coord::ZERO;
		newPosition.x = x;
		newPosition.y = y;
		newPosition.z = z;

		mobility->setCurrentPosition(newPosition);

	} else {
		std::cerr << "Attempt to set position with custom mobility == NULL" << endl;
	}*/
}

/*MobilityROS* RSU::getMobilityModule() {
	//cout << "get Mobility module" << endl;
	
	cModule *host = getContainingNode(this);
	MobilityROS *mobility = check_and_cast<MobilityROS *>(host->getSubmodule("mobility"));
	//IMobility *mobility = check_and_cast<IMobility *>(getParentModule()->getSubmodule("mobility"));
	
	Coord currentPosition = mobility->getCurrentPosition();
	//cout << "OMNET++ Mobility" << endl;
	//cout <<"car:" << car_name <<endl;
	//cout << "x:" << currentPosition.x << endl;
	//cout << "y:" << currentPosition.y << endl;
	//cout << "z:" << currentPosition.z << endl;

	

	return mobility;
}*/

} // namespace artery

