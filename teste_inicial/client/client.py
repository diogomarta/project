import cv2
import numpy as np
import socket
import sys
import pickle
import struct
import time
from datetime import datetime
from vidgear.gears import VideoGear

cap = VideoGear(source='input.mp4').start()
clientsocket=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#clientsocket.connect(('192.168.1.2',8089))
clientsocket.connect(('13.211.135.170',8089))

def write_lat(lat):
	f=open("lat.txt","a")
	f.write(str(lat*1000)+"\n")
	f.close

while True:
	frame=cap.read()
	data = pickle.dumps(frame)
	message_size = struct.pack("L", len(data))
	clientsocket.sendall(message_size + data)
	now=datetime.now()
	time1=now.strftime("%S.%f")
	time1=float(time1)
	
	lines=clientsocket.recv(1024)

	now=datetime.now()
	time2=now.strftime("%S.%f")
	time2=float(time2)
	
	lat=time2-time1
	write_lat(lat)
	lines_arr=pickle.loads(lines)
	print(repr(lines_arr))
